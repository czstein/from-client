#!/bin/sh

echo "Installing from-client..."
git clone https://gitlab.com/czstein/from-client
sudo mkdir -p /usr/local/bin
sudo cp from-client/from-client /usr/local/bin/
rm -rf from-client
echo "Installed! Run with from-client"
